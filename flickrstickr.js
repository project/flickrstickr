// Have to count the pages
var pagenum = 1;
var savedresult = '';
// For AJAX wizard
function getHTTPObject() {
  var xmlhttp;

  /*@cc_on

  @if (@_jscript_version >= 5)

  try {

  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");

  } catch (e) {

  try {

  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

  } catch (E) {

  xmlhttp = false;

  }

  }

  @else

  xmlhttp = false;

  @end @*/

  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}

function handlePhotosRequest() {
  if (photosRequest.readyState == 2) {
    document.getElementById("flickrstickr_loading").innerHTML="Loading photos...";
  }
  if (photosRequest.readyState == 4) {
    document.getElementById("flickrstickr_loading").innerHTML="";
    xmldoc =photosRequest.responseXML.documentElement;
    if (xmldoc.getAttribute("stat")=="fail"){
      err = xmldoc.getElementsByTagName("err");
      alert(err[0].getAttribute("msg"));
      return;
    }
    photos=xmldoc.getElementsByTagName("photo");
    savedresult = photos;
    for (i = 0; i < photos.length; i++) {
      img = document.getElementById("flickrstickrphoto"+i);
      // Need to pass parameters by url because tinymce use full url
      var size_popup = document.getElementById('flickrstickr_size_popup').value;
      size_popup = size_popup.substring(1, size_popup.length);
      size = new String(document.getElementById('flickrstickr_size').value);
      size = size.substring(1, size.length);
            var imgparams = document.getElementById('flickrstickr_align').value
                      +'_'+size
                      +'_'+size_popup;
      img.src="http://static.flickr.com/"+photos[i].getAttribute("server")
                                         +"/"+photos[i].getAttribute("id")
                                         +"_"+photos[i].getAttribute("secret")+"_s.jpg#"
                                         +imgparams;
      img.style.display="inline";
      images[i].id=photos[i].getAttribute("id");
      images[i].server=photos[i].getAttribute("server");
      images[i].secret=photos[i].getAttribute("secret");
      images[i].owner=photos[i].getAttribute("owner");

    }
    for (i = photos.length; i < getMaxImageNo(); i++) {
      image = document.getElementById("flickrstickrphoto"+i);
      image.style.display="none";
    }
    // If don't have any result hide the navibuttons. Otherwise show.
    if (photos.length != 0) {
      document.getElementById('navnext').style.display='inline';
      document.getElementById('navprev').style.display='inline';
    }
    else {
      document.getElementById('navnext').style.display='none';
      document.getElementById('navprev').style.display='none';
      document.getElementById("flickrstickr_loading").innerHTML="No images found";
    }
  }
}
	
function getPhotosFromName(username) {
  userRequest.open("GET", getBaseUrl()+"/index.php?q=flickrstickr/xml/user/"+username+"/"+pagenum,true);
  userRequest.onreadystatechange = handleUserRequest;
  userRequest.send(null);
}

function handleUserRequest() {
  if (photosRequest.readyState == 2) {
    document.getElementById("flickrstickr_loading").innerHTML="Loading user...";
  }
  if (userRequest.readyState == 4) {
    document.getElementById("flickrstickr_loading").innerHTML="";
    xmldoc =userRequest.responseXML.documentElement;
    if (xmldoc.getAttribute("stat")=="fail") {
      err = xmldoc.getElementsByTagName("err");
      alert(err[0].getAttribute("msg"));
      return;
    }
    user = xmldoc.getElementsByTagName("user");
    username = user[0].getElementsByTagName("username");
    userids[username[0].textContent] = user[0].getAttribute("id");
    getPhotosFromId(user[0].getAttribute("id"));
  }
}

function getPhotosFromId(userid) {
  tags = document.getElementById("flickrstickr_tag").value;
  photosRequest.open("GET", getBaseUrl()+"/index.php?q=flickrstickr/xml/images/"+userid+"/"+tags+"/"+pagenum,true);
  photosRequest.onreadystatechange = handlePhotosRequest;
  photosRequest.send(null);
}

function loadImages(){
  username = document.getElementById("flickrstickr_user").value;
  tags = document.getElementById("flickrstickr_tag").value;
  if (username=="" && tags=="") {
    getPhotosFromId("!recent!",tags);
  }
  else if (username=="" && tags!="") {
    getPhotosFromId("all",tags);
  }
  else if (userids[username]!=null) {
    getPhotosFromId(userids[username],tags);
  }
  else {
    getPhotosFromName(username);
  }
}

function prevPage() {
  if (pagenum == 1) {
    alert('Already at the first page');
  }
  else {
    pagenum--;
    loadImages();
  }
}

function nextPage() {
  pagenum++;
  loadImages();
}

function insertAtCursor(field, value) {
  if (document.selection) { // for IE or compatible
    field.focus();
    sel = document.selection.createRange();
    sel.text = value;
  }
  else if (field.selectionStart || field.selectionStart == '0') {   // for FF, Moz, Konq, etc
    var start = field.selectionStart;
    var end = field.selectionEnd;
    field.value = field.value.substring(0, start) + value + field.value.substring(end, field.value.length);
  } else {  // For dummy browsers
    field.value += value;
  }
}

function updateImages() {
  for (i = 0; i < savedresult.length; i++) {
    img = document.getElementById("flickrstickrphoto"+i);
    // Need to pass parameters by url because tinymce use full url
    var size_popup = document.getElementById('flickrstickr_size_popup').value;
    size_popup = size_popup.substring(1, size_popup.length);
    size = new String(document.getElementById('flickrstickr_size').value);
    size = size.substring(1, size.length);
    var imgparams = document.getElementById('flickrstickr_align').value
                    +'_'+size
                    +'_'+size_popup;
    img.src="http://static.flickr.com/"+photos[i].getAttribute("server")
                                       +"/"+photos[i].getAttribute("id")
                                       +"_"+photos[i].getAttribute("secret")+"_s.jpg#"
                                       +imgparams;
      img.style.display="inline";
      images[i].id=photos[i].getAttribute("id");
      images[i].server=photos[i].getAttribute("server");
      images[i].secret=photos[i].getAttribute("secret");
      images[i].owner=photos[i].getAttribute("owner");

    }
}

function toggleSelector() {
  var selectorElement = document.getElementById('flickrstickr_float_selector');
  var selectorButton = document.getElementById('flickr_togglebutton');
  if (selectorElement.style.display != 'none') {
    selectorElement.style.display='none';
    selectorButton.value='Show the image selector';
  }
  else {
    selectorElement.style.display='block';
    selectorButton.value='Hide the image selector';
  }
}

function dockSelector() {
  var selectorElement = document.getElementById('flickrstickr_float_selector');
  var selectorButton = document.getElementById('flickr_dockbutton');
  if (selectorElement.style.position != 'relative') {
    selectorElement.style.position = 'relative';
    selectorElement.style.top = '0px';
    selectorElement.style.left = '0px';
    selectorButton.value='UnDock the image selector';
  } else {
    selectorElement.style.position = 'fixed';
    selectorElement.style.top = '0px';
    selectorElement.style.left = '20%';
    selectorButton.value='Dock the image selector';
  }
}

function handleEnter(evt) {
  if (window.event && window.event.keyCode == 13) {
    loadImages();
    return false;
  }
  
  if (evt.keyCode == 13) {
    loadImages();
    return false;
  }
}